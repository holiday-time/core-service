const express = require("express")
const app = express()
const fs = require('fs')

app.use(express.json())

app.get("/holidays", function(req, res){
    console.log("incoming req")
    const raw_data = fs.readFileSync('./holidays.json')
    res.send(JSON.parse(raw_data))
})

app.listen(10001, function(){
    console.log("app running on port 10001")
})